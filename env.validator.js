module.exports = function validate(variables) {
    for (const env in variables) {
        const envConf = variables[env];

        if (envConf.required) {
            if (!process.env[env]) {
                if (envConf.if_not && process.env[envConf.if_not]) {
                    continue;
                }

                console.error(`${env} environemnt variable is required`);
                process.exit(1);
            }
        }

        if (
            process.env[env] &&
            envConf.allowed_values &&
            !envConf.allowed_values.includes(process.env[env])
        ) {
            console.error(
                `${
                process.env[env]
                } is not allowed for ${env}, should be one of ${envConf.allowed_values.join(
                    ", "
                )}`
            );
            process.exit(1);
        }
    }
};
