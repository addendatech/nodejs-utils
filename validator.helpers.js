const Response = require('./response.helpers');

const regexes = {
    password: {
        regex: /^(?=.*\d)(?=.*[a-z])(?=.*[a-zA-Z])(?=.*[!@#\$%\^&\*]).{8,}$/,
        message: 'Password should be at least 8 characters and must contain 1 lowercase, 1 number and 1 special character(!@#$%^&*)'
    }
};

module.exports = class Validator {
    static regexs(type) {
        return regexes[type] ? regexes[type] : regexes;
    }

    /**
     * Set regex item
     * @param {String} key 
     * @param {Object} value
     * @param {RegExp} value.regex
     * @param {String} value.message
     */
    static setRegex(key, value) {
        regexes[key] = value;
    }

    static validate(schema, data, options = {}) {
        const response = schema.validate(data, Object.assign({
            abortEarly: false,
            stripUnknown: true
        }, options['joi']));

        let errors = null;

        if (response.error) {
            errors = {};

            for (const err of response.error.details) {
                errors[err.context.key] = [err.message];
            }
        }

        if (errors && options['exception'] === true && options['return_promise'] === false) {
            throw Response.error(422, 'Input validation error', errors);
        }

        if (options['return_promise'] === false) {
            return {
                joi: response,
                errors: errors
            };
        }

        if (errors) {
            return Promise.reject(Response.error(422, 'Input validation error', errors));
        }

        return Promise.resolve(response.value);
    };
}
