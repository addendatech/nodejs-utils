const util = require('util');

const options = {
    callbacks: {
        error_handler: null
    }
};

class Response {
    // Methods
    /**
     * Add callback
     * @param {String} type - Type of callback
     * @param {Function(Object)} callback - Callback function
     */
    static addCallback(type, callback) {
        if (typeof options.callbacks[type] === 'undefined') {
            throw Response.error(500, `${type} callback is not supported!`);
        }

        if (typeof callback !== 'function') {
            throw Response.error(500, `Callback should be a valid function!`);
        }

        options.callbacks[type] = callback;
    }

    static success(res, message, options = {}) {
        return Response.construct_response(res, 200, message, null, options);
    }

    static error_bad_request(res, message = 'Bad request', options = {}) {
        return Response.construct_response(res, 400, message, null, options);
    }

    static error_not_authorized(res, message = 'Not authorized', options = {}) {
        return Response.construct_response(res, 401, message, null, options);
    }

    static error_forbidden(res, message = 'You don\'t have enough permissions to access this', options = {}) {
        return Response.construct_response(res, 403, message, null, options);
    }

    static error_not_found(res, message = 'The resource you are looking for is not found', options = {}) {
        return Response.construct_response(res, 404, message, null, options);
    }

    static error_method_not_allowed(res, message = 'Method not allowed for this resource', options = {}) {
        return Response.construct_response(res, 405, message, null, options);
    }

    static error_not_acceptable(res, message = 'Headers are not valid', options = {}) {
        return Response.construct_response(res, 406, message, null, options);
    }

    static error_request_timeout(res, message = 'Request timeout', options = {}) {
        return Response.construct_response(res, 408, message, null, options);
    }

    static error_conflict(res, message = 'Conflict', options = {}) {
        return Response.construct_response(res, 409, message, null, options);
    }

    static error_gone(res, message = 'Resource is not available', options = {}) {
        return Response.construct_response(res, 410, message, null, options);
    }

    static error_unprocessable_entity(res, message = 'Input validation failed', errors, options = {}) {
        return Response.construct_response(res, 422, message, errors, options);
    }

    static error_too_many_requests(res, message = 'Too many requests', options = {}) {
        return Response.construct_response(res, 429, message, null, options);
    }

    static error_too_early(res, message = 'Too early', options = {}) {
        return Response.construct_response(res, 425, message, null, options);
    }

    static error_invalid_token(res, message = 'Invalid Token', options = {}) {
        return Response.construct_response(res, 498, message, null, options);
    }

    static error_token_required(res, message = 'Token is required to make this call', options = {}) {
        return Response.construct_response(res, 499, message, null, options);
    }

    static error_internal_server_error(res, message = 'Internal server error', options = {}) {
        return Response.construct_response(res, 500, message, null, options);
    }

    static error_not_implemented(res, message = 'Not implemented', options = {}) {
        return Response.construct_response(res, 501, message, null, options);
    }

    static error_bad_gateway(res, message = 'Bad gateway', options = {}) {
        return Response.construct_response(res, 502, message, null, options);
    }

    static error_service_unavailable(res, message = 'Service unavailable', options = {}) {
        return Response.construct_response(res, 503, message, null, options);
    }

    static error_gateway_timeout(res, message = 'Gateway timeout', options = {}) {
        return Response.construct_response(res, 504, message, null, options);
    }

    static error(code, message, errors, options = {}) {
        return new ResponseError(code, message, errors, options);
    }

    static construct_response(res, code, message, errors, options = {}) {
        const response = { status_code: code, message: message };

        if (options['data']) {
            response[options['data_key'] ? options['data_key'] : 'data'] = options['data'];
        }

        if (code !== 200 && (typeof options['exception'] === 'undefined' || options['exception'] === true)) {
            throw Response.error(code, message, errors, options);
        }

        return res.status(response.status_code).json(response);
    }

    static handle_errors(req, res, e) {
        if (e) {
            return ErrorHandler.handle(req, res)(e);
        }

        return ErrorHandler.handle(res);
    }

    static addErrorStack(req, error) {
        ErrorHandler.addErrorStack(req, error);
    }
}

class ResponseError extends Error {
    constructor(code, message, errors = null, options = { detail, data }) {
        super(message);
        this.status_code = code;
        this.errors = errors;
        this.options = options;
    }

    json() {
        let response = { status_code: this.status_code, message: this.message };

        if (this.errors) {
            response['errors'] = this.errors;
        }

        if (this.options.data) {
            response[this.options['data_key'] ? this.options['data_key'] : 'data'] = this.options.data;
        }

        if (this.options.detail) {
            response.detail = this.options.detail;
        }

        return response;
    }

    static fromError(e) {
        return ResponseError(e.code || 500, e.message || e);
    }
}

class ErrorHandler {
    static handle(req, res) {
        return (e) => {
            if (e) {
                if (ErrorHandler.logError(e)) {
                    ErrorHandler.log(req, e);
                }

                if (e.constructor && e.constructor.name === 'MongooseServerSelectionError') {
                    res.status(500).json({ status_code: 500, message: 'Something went wrong, please try again.', detail: 'DB connection issue' });
                    return;
                }

                if (typeof e.json === 'function') {
                    res.status(e.status_code).json(e.json());
                } else {
                    const parsed = ErrorHandler.messageParser(typeof e === 'string' ? e : e.message);
                    res.status(parsed.status_code).json(parsed);
                }
            } else {
                res.status(500).json({ status_code: 500, message: 'Something went wrong!' });
            }
        }
    }

    static messageParser(message) {
        const messagePatterns = [
            {
                pattern: 'Cast to ObjectId failed for value "([0-9a-z]+)" at path "([a-zA-Z_]+)" for model "(.*?)"',
                code: 400,
                callback: (match) => {
                    return `'${match[1]}' is not valid`;
                }
            }
        ];
        let response = { status_code: 400, message: message };

        for (const p of messagePatterns) {
            const match = message.match(new RegExp(p.pattern, 'i'));

            if (match) {
                if (typeof p.callback === 'function') {
                    response = { status_code: p.code || response.status_code, message: p.callback(match) };
                } else {
                    response = { status_code: p.code || response.status_code, message: util.format(p.message, match[1] ? match[1].trim() : '') };
                }

                break;
            }
        }

        return response;
    }

    static logError(e) {
        return true;
    }

    static addErrorStack(req, error) {
        req.logs.push(error);
    }

    static log(req, e) {
        if (options.callbacks.error_handler) {
            options.callbacks.error_handler({
                route: req.originalUrl,
                ip: req.connection.remoteAddress,
                x_real_ip: req.headers['x-real-ip'],
                source: req.source,
                query: req.query,
                payload: req.body,
                logStack: req.logs,
                exception: e,
                error: typeof e && e.json === 'function' ? e.json() : null
            });
        } else {
            console.error('--------------------------------------------------------------------------');
            console.error('Route:', req.originalUrl);
            console.error('IP:', req.connection.remoteAddress);
            console.error('X-Real-IP:', req.headers['x-real-ip']);

            if (req.source) {
                console.error('Source:', req.source);
            }

            console.error('Query:', req.query);
            console.error('Payload:', req.body);

            if (req.logs && Array.isArray(req.logs)) {
                req.logs.forEach(item => {
                    console.error(...(Array.isArray(item) ? item : [item]));
                });
            }

            console.error('Error:', e);
            console.error('--------------------------------------------------------------------------');
        }
    }
};

module.exports = Response;
