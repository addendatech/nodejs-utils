const Response = require('./response.helpers');
const Readable = require('stream').Readable;
const FormData = require('form-data');
const axios = require('axios').default;

/**
 * Generate random string
 * @param {Number} length - Length of the random string
 * @param {Object} options - Options
 * @param {String} options.type - Type of 
 * @param {String} options.prefix - To prefix
 * @param {String} options.postfix - To postfix
 * @param {String} options.characters - Characters to include only
 * @param {String} options.alpha_chars - Alpha characteres to allow
 * @param {String} options.numeric_chars - Numeric characteres to allow
 * @param {String} options.special_chars - Special characteres to allow
 */
const generateRandomString = (length = 32, options = {}) => {
    options = {
        type: 'alpha|numeric|special',
        alpha_chars: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
        numeric_chars: '0123456789',
        special_chars: `" !"#$%&'()*+,-./:;<=>?@[\]^_\`{|}~"`,
        ...options
    };
    const characterSets = {
        alpha: options.alpha_chars,
        numeric: options.numeric_chars,
        special: options.special_chars
    };

    if (options.prefix && typeof options.prefix === 'string' && options.prefix.trim()) {
        options.prefix = options.prefix.trim();
        length -= options.prefix.length;
    }

    if (options.postfix && typeof options.postfix === 'string' && options.postfix.trim()) {
        options.postfix = options.postfix.trim();
        length -= options.postfix.length;
    }

    let possible = '';

    if (options.characters) {
        possible = options.characters;
    } else {
        for (const cs of options.type.split('|')) {
            possible += characterSets[cs];
        }
    }

    let string = '';

    for (let i = 0; i < length; i++) {
        string += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return `${options.prefix ? options.prefix : ''}${string}${options.postfix ? options.postfix : ''}`;
};

function generateRandomNumber(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const generateID = (m = Math, d = Date, h = 16, s = s => m.floor(s).toString(h)) => {
    return s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h));
}

function getDeadline(date, interval, options = {}) {
    let deadlineDate = null;
    const startDate = new Date(date.getTime());
    const unit_type = interval[interval.length - 1];
    const unit_to_add = parseInt(interval.replace(/[a-z]/, ""));
    const isWeekend = date => {
        return options.weekends ? options.weekends.includes(date.getDay()) : false;
    };

    while (isWeekend(startDate)) {
        startDate.setDate(startDate.getDate() + 1);
    }

    if (unit_type === 'd') {
        let days = 0;

        while (days < unit_to_add) {
            deadlineDate = new Date(startDate.setDate(startDate.getDate() + 1));

            if (!isWeekend(deadlineDate)) {
                days++;
            }
        }
    } else if (unit_type === 'h') {
        deadlineDate = new Date(startDate.setHours(startDate.getHours() + unit_to_add));
    } else if (unit_type === 'm') {
        deadlineDate = new Date(startDate.setMinutes(startDate.getMinutes() + unit_to_add));
    }

    return deadlineDate;
}

/**
 * Verify Recaptcha code
 * @param {String} captcha - Captcha code
 * @param {Object} options - Options
 * @param {String} options.secret - Recaptcha Secret
 * @param {Boolean} options.throw_error_on_failure - Throw error on failure?
 * @returns {Promise<Object>}
 */
async function verifyRecaptcha(captcha, options = {}) {
    const data = new FormData();
    data.append('secret', options.secret ? options.secret : process.env.GOOGLE_RECAPTCHA_SECRET);
    data.append('response', captcha);

    return axios.post('https://www.google.com/recaptcha/api/siteverify', data, { headers: data.getHeaders() }).then(response => {
        if (!response.data.success && options.throw_error_on_failure !== false) {
            throw Response.error(422, 'Input validation error', { captcha: ['Captcha is not valid'] });
        }

        return response.data;
    }).catch(e => {
        throw Response.error(422, 'Input validation error', { captcha: ['Captcha is not valid'] });
    });
}

function getFileExtension(name) {
    const matches = name.match(/(?:.*)\.(.*)/);

    if (matches) {
        return matches[1];
    }
}

function deepmerge(source, object, options = {
    skip_nested,
    ignore_keys,
    beforeKeyMerge
}, keys = []) {
    for (const prop in object) {
        key = `${keys.length ? keys.join('.') + '.' : ''}${prop}`;

        if (typeof options.beforeKeyMerge === 'function') {
            options.beforeKeyMerge(source, object, key, prop);
        }

        if (Array.isArray(options.ignore_keys) && options.ignore_keys.length) {
            if (options.ignore_keys.includes(key)) {
                continue;
            }
        }

        if (Array.isArray(options.skip_nested) && options.skip_nested.length) {
            if (options.skip_nested.includes(key)) {
                source[prop] = object[prop];
                continue;
            }
        }

        if (object[prop] !== null && typeof object[prop] === 'object') {
            keys.push(prop);

            if (!source[prop]) {
                source[prop] = object[prop];
            } else {
                deepmerge(source[prop], object[prop], options, keys);
            }

            keys.pop();
        } else {
            source[prop] = object[prop];
        }
    }
}

function removeSpecialCharacters(string) {
    if (string) {
        return string.replace(/[^a-zA-Z0-9]/g, '');
    }

    return string;
}

function calculateAgeFromDOB(dob) {
    var today = new Date();

    if (typeof dob === 'string') {
        dob = new Date(dob);
    }

    var age = today.getFullYear() - dob.getFullYear();
    var m = today.getMonth() - dob.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < dob.getDate())) {
        age--;
    }

    return age;
}

/**
 * Convert base64 file buffer or string to ReadStream
 * @param {String|Buffer} file - File string
 * @returns {Readable} ReadStream - ReadStream
 */
function base64ToReadStream(content) {
    if (content.constructor.name !== 'Buffer') {
        content = Buffer.from(content, 'base64');
    }

    return new Readable({
        read() {
            this.push(content);
            this.push(null);
        }
    });
}

/**
 * Calculate age based on year or date
 * @param {Date|Number} value - Either date or year
 * @param {Boolean} fromYear - Convert from year
 * @param {Object} options - options
 * @param {Boolean} options.include_passed_year - Include passed year in the calculation
 * @returns {Number}
 */
function calculateAge(value, fromYear = false, options = {}) {
    if (value) {
        var today = new Date();
        var birthDate = new Date(fromYear ? `${value}` : value);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();

        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }

        if (fromYear && options.include_passed_year === true) {
            age++;
        }

        return age;
    }

    return value;
}

module.exports = {
    generateRandomString,
    generateRandomNumber,
    generateID,
    getDeadline,
    getFileExtension,
    verifyRecaptcha,
    deepmerge,
    removeSpecialCharacters,
    calculateAgeFromDOB,
    base64ToReadStream,
    calculateAge
};
