let routeCallback = null;

/**
 * Register callback for route
 * @param {Function(req Object)} cb - Callback
 */
function registerRouteCallback(cb) {
    routeCallback = cb;
}

const route = (options = { routeName }) => (req, res, next) => {
    req.logs = [];

    if (options.routeName) {
        req.routeName = options.routeName;
    }

    if (typeof routeCallback === 'function') {
        routeCallback(req);
    }

    next();
};

module.exports = {
    route,
    registerRouteCallback
};
